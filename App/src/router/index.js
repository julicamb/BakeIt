import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Categories from '@/components/Categories'
import Recipes from '@/components/Recipes'
import Category from '@/components/CategoryShow'
import Recipe from '@/components/RecipeShow'
import CategoryCreate from '@/components/CategoryCreate'
import RecipeCreate from '@/components/RecipeCreate'
import CategoryEdit from '@/components/CategoryEdit'
import RecipeEdit from '@/components/RecipeEdit'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/categories',
      name: 'Categories',
      component: Categories
    },
    {
      path: '/recipes',
      name: 'Recipes',
      component: Recipes
    },
    {
      path: '/category/:id',
      name: 'Category',
      component: Category
    },
    {
      path: '/recipe/:id',
      name: 'Recipe',
      component: Recipe
    },
    {
      path: '/categorycreate',
      name: 'New Category',
      component: CategoryCreate
    },
    {
      path: '/recipecreate',
      name: 'New Recipe',
      component: RecipeCreate
    },
    {
      path: '/categoryedit/:id',
      name: 'CategoryEdit',
      component: CategoryEdit
    },
    {
      path: '/recipeedit/:id',
      name: 'RecipeEdit',
      component: RecipeEdit
    }
  ]
})
